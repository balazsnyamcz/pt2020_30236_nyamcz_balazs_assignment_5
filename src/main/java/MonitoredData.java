import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredData {
    private static SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Date start_time;
    private Date end_time;
    private String activity_label;

    public MonitoredData(Date start_time, Date end_time, String activity_label) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity_label = activity_label;
    }

    private static void process(String line,ArrayList<MonitoredData> monitoredData){
        String[] spl=line.split("\t\t");
        spl[2]=spl[2].replaceAll("\\s+","");
        try {
            monitoredData.add(new MonitoredData(format.parse(spl[0]),format.parse(spl[1]),spl[2]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<MonitoredData> task_1(String fname) {
        ArrayList<MonitoredData> monitoredData = new ArrayList<>();
        try (Stream<String> lines = Files.lines(Paths.get(fname))) {
            lines.forEachOrdered(line -> process(line, monitoredData));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return monitoredData;
    }

    public static Map<Long,List<MonitoredData>> task_2(ArrayList<MonitoredData> monitoredData){
        return monitoredData.stream().collect(Collectors.groupingBy(p->p.start_time.getTime()/(1000*60*60*24)));
    }

    public static Map<String,Integer> task_3(ArrayList<MonitoredData> monitoredData){
        return monitoredData.stream()
                    .collect(Collectors.toMap(s->s.activity_label,s->1,Integer::sum));
    }

    public static Map<Integer, Map<String, Integer>> task_4(ArrayList<MonitoredData> monitoredData){
        return monitoredData.stream()
                .collect(Collectors.groupingBy(p->Integer.parseInt(String.valueOf(p.getStart_time().getTime()/(1000*60*60*24))),
                        Collectors.toMap(MonitoredData::getActivity_label, p->1,Integer::sum)));
    }

    public static Map<String, Integer> task_5(ArrayList<MonitoredData> monitoredData){
        return monitoredData.stream()
                .collect(Collectors.toMap(MonitoredData::getActivity_label,
                        p->Integer.parseInt(String.valueOf((p.getEnd_time().getTime()-p.getStart_time().getTime())/(1000*60))),
                        Integer::sum));
    }

    public static List<String> task_6(ArrayList<MonitoredData> monitoredData){
        Map<String,Integer> activityDurationLessThen5Min =monitoredData.stream()
                .filter(p->(p.getEnd_time().getTime()-p.getStart_time().getTime())/(1000*60)<5)
                .collect(Collectors.toMap(MonitoredData::getActivity_label, s->1,Integer::sum))
                ;
        Map<String,Integer> activitiesAppearances=task_3(monitoredData);

        List<String> activities=new ArrayList<>();

        activitiesAppearances.forEach((key,nr)-> {
            if(activityDurationLessThen5Min.containsKey(key)) {
                if(activityDurationLessThen5Min.get(key)/(double)nr>0.9){
                    activities.add(key);
                }
            }
        });
        return activities;
    }

    public Date getStart_time() {
        return start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public String getActivity_label() {
        return activity_label;
    }

    @Override
    public String toString() {
        return "{"+ start_time +", "+ end_time +", "+ activity_label +"}"+"\n";
    }

    public static void write(String fname,String data){
        FileWriter myWriter;
        try {
            myWriter = new FileWriter(fname);
            myWriter.write(data);
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args){
        ArrayList<MonitoredData> monitoredData= task_1(args[0]);
        write("Task_1.txt",monitoredData.toString());

        Map<Long,List<MonitoredData>> distinctDays=task_2(monitoredData);
        write("Task_2.txt",String.valueOf(distinctDays.size()));

        Map<String,Integer> activitiesAppearances=task_3(monitoredData);
        write("Task_3.txt",String.valueOf(activitiesAppearances));

        Map<Integer, Map<String, Integer>> acticitiesEachDay=task_4(monitoredData);
        write("Task_4.txt",String.valueOf(acticitiesEachDay));

        Map<String, Integer> activityDuration=task_5(monitoredData);
        write("Task_5.txt",String.valueOf(activityDuration));

        List<String> activities=task_6(monitoredData);
        write("Task_6.txt",String.valueOf(activities));
    }
}
